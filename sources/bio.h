#pragma once

#include "genetic_string.h"

namespace bio {
const std::string nucleobases{"ACGT"};
const std::string start_codon{"AUG"};
const std::vector<std::string> stop_codons{"UAG", "UGA", "UAA"};
const std::unordered_map<std::string, char> rna_codon_table{
    {"UUU", 'F'}, {"CUU", 'L'}, {"AUU", 'I'}, {"GUU", 'V'}, {"UUC", 'F'},
    {"CUC", 'L'}, {"AUC", 'I'}, {"GUC", 'V'}, {"UUA", 'L'}, {"CUA", 'L'},
    {"AUA", 'I'}, {"GUA", 'V'}, {"UUG", 'L'}, {"CUG", 'L'}, {"AUG", 'M'},
    {"GUG", 'V'}, {"UCU", 'S'}, {"CCU", 'P'}, {"ACU", 'T'}, {"GCU", 'A'},
    {"UCC", 'S'}, {"CCC", 'P'}, {"ACC", 'T'}, {"GCC", 'A'}, {"UCA", 'S'},
    {"CCA", 'P'}, {"ACA", 'T'}, {"GCA", 'A'}, {"UCG", 'S'}, {"CCG", 'P'},
    {"ACG", 'T'}, {"GCG", 'A'}, {"UAU", 'Y'}, {"CAU", 'H'}, {"AAU", 'N'},
    {"GAU", 'D'}, {"UAC", 'Y'}, {"CAC", 'H'}, {"AAC", 'N'}, {"GAC", 'D'},
    {"UAA", '.'}, {"CAA", 'Q'}, {"AAA", 'K'}, {"GAA", 'E'}, {"UAG", '.'},
    {"CAG", 'Q'}, {"AAG", 'K'}, {"GAG", 'E'}, {"UGU", 'C'}, {"CGU", 'R'},
    {"AGU", 'S'}, {"GGU", 'G'}, {"UGC", 'C'}, {"CGC", 'R'}, {"AGC", 'S'},
    {"GGC", 'G'}, {"UGA", '.'}, {"CGA", 'R'}, {"AGA", 'R'}, {"GGA", 'G'},
    {"UGG", 'W'}, {"CGG", 'R'}, {"AGG", 'R'}, {"GGG", 'G'}};
const std::unordered_map<char, double> monoisotopic_mass_table{
    {'A', 71.03711},  {'C', 103.00919}, {'D', 115.02694}, {'E', 129.04259},
    {'F', 147.06841}, {'G', 57.02146},  {'H', 137.05891}, {'I', 113.08406},
    {'K', 128.09496}, {'L', 113.08406}, {'M', 131.04049}, {'N', 114.04293},
    {'P', 97.05276},  {'Q', 128.05858}, {'R', 156.10111}, {'S', 87.03203},
    {'T', 101.04768}, {'V', 99.06841},  {'W', 186.07931}, {'Y', 163.06333}};

std::vector<int> count_nucleobases(const std::string& dna);

std::string get_rna(const std::string& dna);

char get_complement(char c);

std::string get_reverse_complement(const std::string& dna);

int get_hamming_distance(const std::string& s1, const std::string& s2);

std::vector<int> get_dna_pattern_positions(const std::string& dna,
                                           const std::string& dna_pattern);

std::string get_protein(const std::string& rna);

double get_dominant_allele_probability(int homozygous_dominant,
                                       int heterozygous,
                                       int homozygous_recessive);

bool is_reverse_palindrome(const std::string& dna);

std::vector<std::string> get_longest_common_substrings(
    const std::vector<genetic_string>& genetic_strings);

std::unordered_set<std::string> get_open_reading_frame_proteins(
    const std::string& dna);

std::unordered_map<char, std::vector<int>> get_profile(
    const std::vector<genetic_string>& genetic_strings);

std::string get_consensus(
    const std::unordered_map<char, std::vector<int>>& profile);

std::string get_protein_fasta_url(const std::string& protein_id);

bool are_overlaping(const std::string& s1, const std::string& s2, int k);

std::vector<std::pair<std::string, std::string>> get_adjacency_list(
    const std::vector<genetic_string>& genetic_strings, int k);

double get_transition_transversion_ratio(const std::string& s1,
                                         const std::string& s2);

double get_proportion_distance(const std::string& s1, const std::string& s2);
}
