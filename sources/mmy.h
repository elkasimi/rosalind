#pragma once

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace mmy {

template <typename T = int>
std::vector<T> read_vector_n(std::istream& stream = std::cin) {
  int n;
  stream >> n;
  std::vector<T> result(n);
  for (T& i : result) {
    stream >> i;
  }

  return result;
}

template <typename T = int>
std::vector<T> read_vector(std::istream& stream = std::cin) {
  std::vector<T> result;
  for (T i; stream >> i;) {
    result.push_back(i);
  }

  return result;
}

template <typename T, typename Stream>
std::vector<T> read_vector(Stream& stream) {
  std::vector<T> result;
  for (T i; stream >> i;) {
    result.push_back(i);
  }

  return result;
}

template <typename Container>
void print_vector(const Container& container, std::ostream& stream = std::cout,
                  char separator = ' ') {
  bool first = true;
  for (const auto& i : container) {
    if (first) {
      first = false;
    } else {
      stream << separator;
    }
    stream << i;
  }
  stream << "\n";
}

///////// Element iterator ////////////
template <typename Iterator>
struct range {
  range(Iterator first, Iterator last) : first(first), last(last) {}

  auto begin() const { return first; }

  auto end() const { return last; }

  Iterator first, last;
};

template <typename Iterator>
auto make_range(Iterator first, Iterator last) {
  return range<Iterator>(first, last);
}

template <typename Iterator>
struct ElementIterator {
  ElementIterator(Iterator iterator, Iterator end)
      : iterator(iterator), end(end) {}

  operator bool() const { return iterator != end; }

  auto operator*() const { return *iterator; }

  operator Iterator() const { return iterator; }

  void operator++() { ++iterator; }

  auto next() const {
    auto n = iterator;
    if (n != end) {
      ++n;
    }

    return ElementIterator{n, end};
  }

  auto range() const { return make_range(iterator, end); }

  Iterator iterator;
  Iterator end;
};

template <typename Iterator>
auto make_element_iterator(Iterator first, Iterator last) {
  return ElementIterator<Iterator>{first, last};
}

template <typename Container, typename InputIterator>
auto position(const Container& container, InputIterator iterator) {
  return distance(container.cbegin(),
                  static_cast<typename Container::const_iterator>(iterator));
}

template <typename Container1, typename Container2>
auto search(const Container1& container1, const Container2& container2) {
  auto iterator = search(container1.begin(), container1.end(),
                         container2.begin(), container2.end());
  return make_element_iterator(iterator, container1.end());
}

//////////// algorithms ///////////
template <typename Container>
void reverse(Container& container) {
  std::reverse(container.begin(), container.end());
}

template <typename Container, typename UnaryPredicate>
auto count_if(const Container& container, UnaryPredicate p) {
  return std::count_if(container.begin(), container.end(), p);
}

template <typename Container, typename T, typename BinaryOperation>
auto accumulate(const Container& container, T init, BinaryOperation op) {
  return std::accumulate(container.begin(), container.end(), init, op);
}

template <typename Container>
bool next_permutation(Container& container) {
  return std::next_permutation(container.begin(), container.end());
}

template <typename Container, typename Predicate>
bool all_of(const Container& container, Predicate p) {
  return std::all_of(container.begin(), container.end(), p);
}

template <typename Container, typename T>
auto find(Container& container, const T& value) {
  return make_element_iterator(
      std::find(container.begin(), container.end(), value), container.end());
}

template <typename Container, typename Predicate>
auto find_if(const Container& container, Predicate p) {
  return std::find_if(container.begin(), container.end(), p);
}

template <typename Container, typename Predicate>
auto any_of(const Container& container, Predicate p) {
  return std::any_of(container.begin(), container.end(), p);
}

template <typename InputConainer, typename OutputContainer,
          typename UnaryOperation>
auto transform(const InputConainer& input, OutputContainer& output,
               UnaryOperation op) {
  return std::transform(input.begin(), input.end(), output.begin(), op);
}

template <typename Container>
struct reverse_range {
  reverse_range(const Container& container) : container(container) {}

  auto begin() { return container.rbegin(); }

  auto end() { return container.rend(); }

  const Container& container;
};

template <typename Container>
auto reversed(Container& container) {
  return reverse_range<Container>(container);
}

//////////// integer range ///////////////
struct xrange {
  struct iterator {
    iterator(int position, int stop, int step)
        : position(position), stop(stop), step(step) {}

    void operator++() {
      if (position < stop) {
        position = std::min(position + step, stop);
      }
    }

    const int& operator*() const { return position; }

    bool operator==(const iterator& rhs) {
      return position == rhs.position and stop == rhs.stop and step == rhs.step;
    }

    bool operator!=(const iterator& rhs) { return !(*this == rhs); }

    int position;
    int stop;
    int step;
  };

  xrange(int start, int stop, int step)
      : start(start), stop(stop), step(step) {}

  xrange(int length) : xrange(0, length, 1) {}

  xrange(int start, int stop) : xrange(start, stop, 1) {}

  iterator begin() const { return iterator{start, stop, step}; }

  iterator end() const { return iterator{stop, stop, step}; }

  int start;
  int stop;
  int step;
};

template <typename T, typename Compare>
std::vector<T> longest_monotonic_sequence(const std::vector<T>& container,
                                          Compare compare) {
  int length = container.size();
  std::vector<int> prev(length, -1);
  std::vector<int> lms(length, 1);
  int max_length = 1;
  int best_end = 0;
  for (int i = 1; i < length; ++i) {
    for (int j = i - 1; j >= 0; --j) {
      if (lms[j] >= lms[i] && compare(container[j], container[i])) {
        lms[i] = lms[j] + 1;
        prev[i] = j;
      }
    }

    if (max_length < lms[i]) {
      max_length = lms[i];
      best_end = i;
    }
  }

  std::vector<T> sequence;
  for (int i = best_end; i != -1; i = prev[i]) {
    sequence.push_back(container[i]);
  }

  reverse(sequence);

  return sequence;
}

template <typename Container>
std::vector<int> longest_common_subsequence(const Container& seq1,
                                            const Container& seq2) {
  int m = seq1.size();
  int n = seq2.size();
  using data = std::tuple<int, int, int>;
  std::vector<std::vector<data>> lcs(m + 1,
                                     std::vector<data>(n + 1, {0, -1, -1}));
  for (int i : xrange(m)) {
    for (int j : xrange(n)) {
      if (seq1[i] == seq2[j]) {
        lcs[i + 1][j + 1] = std::make_tuple(1 + std::get<0>(lcs[i][j]), i, j);
      } else {
        if (std::get<0>(lcs[i][j + 1]) < std::get<0>(lcs[i + 1][j])) {
          lcs[i + 1][j + 1] = lcs[i + 1][j];
        } else {
          lcs[i + 1][j + 1] = lcs[i][j + 1];
        }
      }
    }
  }

  std::vector<int> result;
  for (int i = std::get<1>(lcs[m][n]), j = std::get<2>(lcs[m][n]);
       i != -1 && j != -1;) {
    result.push_back(i);
    int ii = std::get<1>(lcs[i][j]);
    int jj = std::get<2>(lcs[i][j]);
    i = ii;
    j = jj;
  }
  reverse(result);

  return result;
}
}

namespace std {
template <>
struct iterator_traits<mmy::xrange::iterator> {
  using difference_type = ptrdiff_t;
  using iterator_category = forward_iterator_tag;
};
}
