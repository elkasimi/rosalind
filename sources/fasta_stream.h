#pragma once

struct genetic_string;

struct fasta_stream {
  fasta_stream(std::istream& stream);

  operator bool() const;

  std::istream& stream;
  std::string header;
  bool good;
};

fasta_stream& operator>>(fasta_stream& in, genetic_string& the_genetic_string);
