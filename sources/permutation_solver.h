#pragma once

struct permutation_solver {
  permutation_solver(int n, bool negative);

  void find_permutations_dfs(int r);

  void find_all_permutations();

  const auto& get_permutations() const { return permutations; }

  int n;
  bool negative;
  std::vector<int> permutation;
  std::vector<std::vector<int>> permutations;
  std::vector<bool> used;
};
