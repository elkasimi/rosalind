#include "mmy.h"

#include "fasta_stream.h"
#include "genetic_string.h"

using namespace std;
using namespace mmy;

fasta_stream::fasta_stream(istream& stream) : stream(stream), good(true) {}

fasta_stream::operator bool() const { return good; }

fasta_stream& operator>>(fasta_stream& in, genetic_string& the_genetic_string) {
  if (in.header.empty()) {
    if (getline(in.stream, in.header)) {
    } else {
      in.good = false;
      return in;
    }
  }

  the_genetic_string.header = in.header.substr(1);
  the_genetic_string.contents.clear();
  in.header.clear();
  for (string s; getline(in.stream, s);) {
    if (s[0] == '>') {
      in.header = s;
      break;
    }

    the_genetic_string.contents += s;
  }

  return in;
}
