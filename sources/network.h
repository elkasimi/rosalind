#pragma once

namespace network {
std::string get_url_contents(const std::string& url);
}
