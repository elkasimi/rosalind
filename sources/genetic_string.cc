#include "mmy.h"

#include "genetic_string.h"

using namespace std;
using namespace mmy;

double genetic_string::gc_content_ratio() const {
  double ratio = 0.0, total = 0.0;
  for (char c : contents) {
    if (c == 'C' or c == 'G') {
      ratio += 1.0;
    }

    total += 1.0;
  }

  return 100.0 * ratio / total;
}

size_t genetic_string::length() const { return contents.size(); }

ostream& operator<<(ostream& out, const genetic_string& rhs) {
  out << "{header:'" << rhs.header << "', contents:'" << rhs.contents << "'}";
  return out;
}
