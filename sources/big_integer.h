#pragma once

struct big_integer {
  big_integer(const big_integer& rhs);

  big_integer(big_integer&& rvalue);

  big_integer& operator=(big_integer&& rvalue);

  big_integer(const std::string& s);

  big_integer& operator*=(int n);

  big_integer operator*(int n) const;

  big_integer& operator/=(int n);

  big_integer operator/(int n) const;

  big_integer& operator+=(const big_integer& rhs);

  big_integer operator+(const big_integer& rhs) const;

  static big_integer factorial(int n);

  static big_integer combination(int n, int k);

  std::vector<int8_t> data;
};

std::istream& operator>>(std::istream& in, big_integer& bi);

std::ostream& operator<<(std::ostream& out, const big_integer& bi);
