#pragma once

struct genetic_string {
  std::string header;
  std::string contents;

  double gc_content_ratio() const;
  size_t length() const;
};

std::ostream& operator<<(std::ostream& out, const genetic_string& rhs);
