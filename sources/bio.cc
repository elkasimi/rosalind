#include "mmy.h"

#include "bio.h"

using namespace std;
using namespace mmy;

namespace bio {

vector<int> count_nucleobases(const string& dna) {
  int a = 0, c = 0, g = 0, t = 0;
  for (char i : dna) {
    switch (i) {
      case 'A':
        ++a;
        break;
      case 'C':
        ++c;
        break;
      case 'G':
        ++g;
        break;
      case 'T':
        ++t;
        break;
    }
  }

  return {a, c, g, t};
}

string get_rna(const string& dna) {
  string rna = dna;
  for (char& c : rna) {
    if (c == 'T') {
      c = 'U';
    }
  }

  return rna;
}

char get_complement(char c) {
  switch (c) {
    case 'A':
      return 'T';
    case 'T':
      return 'A';
    case 'C':
      return 'G';
    case 'G':
      return 'C';
  }

  return '-';
}

string get_reverse_complement(const string& dna) {
  string reverse_complement = dna;
  mmy::reverse(reverse_complement);
  for (char& c : reverse_complement) {
    c = get_complement(c);
  }

  return reverse_complement;
}

int get_hamming_distance(const string& s1, const string& s2) {
  return count_if(xrange(s1.size()), [&](int i) { return s1[i] != s2[i]; });
}

vector<int> get_dna_pattern_positions(const string& dna,
                                      const string& dna_pattern) {
  vector<int> positions;
  for (auto iterator = mmy::search(dna, dna_pattern); iterator;
       iterator = mmy::search(iterator.next().range(), dna_pattern)) {
    positions.push_back(1 + position(dna, iterator));
  }

  return positions;
}

string get_protein(const string& rna) {
  int length = static_cast<int>(rna.size());
  string protein;
  for (int i : xrange(0, length, 3)) {
    auto iterator = rna_codon_table.find(rna.substr(i, 3));

    if (iterator == rna_codon_table.end()) {
      protein.clear();
      break;
    }

    char c = iterator->second;
    if (c == '.') {
      break;
    }

    protein.append(1, c);
  }

  return protein;
}

double get_dominant_allele_probability(int homozygous_dominant,
                                       int heterozygous,
                                       int homozygous_recessive) {
  return (0.5 * homozygous_dominant * (homozygous_dominant - 1) +
          1.0 * homozygous_dominant * heterozygous +
          1.0 * homozygous_dominant * homozygous_recessive +
          0.375 * heterozygous * (heterozygous - 1) +
          0.5 * heterozygous * homozygous_recessive) /
         (0.5 * (homozygous_dominant + heterozygous + homozygous_recessive) *
          (homozygous_dominant + heterozygous + homozygous_recessive - 1));
}

bool is_reverse_palindrome(const string& dna) {
  for (auto first = dna.begin(), last = std::prev(dna.end()); first < last;
       ++first, --last) {
    if (get_complement(*first) != *last) {
      return false;
    }
  }

  return true;
}

vector<string> get_longest_common_substrings(
    const vector<genetic_string>& genetic_strings) {
  int genetic_strings_count = genetic_strings.size();
  using Map = unordered_map<string, vector<vector<int>>>;
  Map common_substrings;
  for (char c : nucleobases) {
    vector<vector<int>> v(genetic_strings_count);
    for (int i : xrange(genetic_strings_count)) {
      const string& si = genetic_strings[i].contents;
      for (int j : xrange(si.size())) {
        if (si[j] == c) {
          v[i].push_back(j);
        }
      }

      if (v[i].empty()) {
        v.clear();
      }
    }

    if (not v.empty()) {
      common_substrings.emplace(string(1, c), std::move(v));
    }
  }

  for (int count = 1;; ++count) {
    Map next_common_substrings;

    for (auto& p : common_substrings) {
      for (char c : nucleobases) {
        vector<vector<int>> v(genetic_strings_count);
        for (int i : xrange(genetic_strings_count)) {
          for (int j : p.second[i]) {
            int index = j + count;
            const string& si = genetic_strings[i].contents;
            if (index < si.size() and si[index] == c) {
              v[i].push_back(j);
            }
          }

          if (v[i].empty()) {
            v.clear();
            break;
          }
        }

        if (not v.empty()) {
          string s = p.first;
          s.append(1, c);
          next_common_substrings.emplace(std::move(s), std::move(v));
        }
      }
    }

    if (next_common_substrings.empty()) {
      break;
    }

    common_substrings = std::move(next_common_substrings);
  }

  vector<string> longest_common_substrings;
  for (const auto& entry : common_substrings) {
    longest_common_substrings.push_back(std::move(entry.first));
  }

  return longest_common_substrings;
}

unordered_set<string> get_open_reading_frame_proteins(const string& dna) {
  unordered_set<string> proteins;
  string rna = get_rna(dna);
  int length = rna.size();
  for (int offset : xrange(3)) {
    for (int i : xrange(offset, length - 2, 3)) {
      string codon = rna.substr(i, 3);
      if (codon != start_codon) {
        continue;
      }

      bool ok = false;
      string protein{"M"};
      for (int j : xrange(i + 3, length - 2, 3)) {
        string next_codon = rna.substr(j, 3);
        auto iterator = rna_codon_table.find(next_codon);
        char c = iterator->second;
        if (c == '.') {
          ok = true;
          break;
        } else {
          protein.append(1, c);
        }
      }

      if (ok) {
        proteins.insert(protein);
      }
    }
  }

  return proteins;
}

unordered_map<char, vector<int>> get_profile(
    const vector<genetic_string>& genetic_strings) {
  unordered_map<char, vector<int>> profile;

  int length = genetic_strings.front().length();
  for (char c : nucleobases) {
    profile[c] = vector<int>(length, 0);
  }

  for (const genetic_string& gs : genetic_strings) {
    for (int i : xrange(length)) {
      profile[gs.contents[i]][i]++;
    }
  }

  return profile;
}

string get_consensus(const unordered_map<char, vector<int>>& profile) {
  string consensus;
  int length = profile.at('A').size();
  for (int i : xrange(length)) {
    char best = '-';
    int best_occurence = 0;
    for (char c : nucleobases) {
      int occurence = profile.at(c)[i];
      if (best_occurence < occurence) {
        best_occurence = occurence;
        best = c;
      }
    }

    consensus.append(1, best);
  }

  return consensus;
}

string get_protein_fasta_url(const string& protein_id) {
  ostringstream stream;
  stream << "http://www.uniprot.org/uniprot/" << protein_id << ".fasta";

  return stream.str();
}

bool are_overlaping(const string& s1, const string& s2, int k) {
  for (int i : xrange(k)) {
    if (s1[i + s1.size() - k] != s2[i]) {
      return false;
    }
  }

  return true;
}

vector<pair<string, string>> get_adjacency_list(
    const vector<genetic_string>& genetic_strings, int k) {
  vector<pair<string, string>> adjacency_list;
  int length = genetic_strings.size();
  for (int i : xrange(length)) {
    for (int j : xrange(length)) {
      if (i == j) {
        continue;
      }

      if (are_overlaping(genetic_strings[i].contents,
                         genetic_strings[j].contents, k)) {
        adjacency_list.push_back(
            {genetic_strings[i].header, genetic_strings[j].header});
      }
    }
  }

  return adjacency_list;
}

double get_transition_transversion_ratio(const string& s1, const string& s2) {
  int length = s1.size();
  int transitions = 0;
  int transversions = 0;

  for (int i = 0; i < length; ++i) {
    if (s1[i] == s2[i]) {
      continue;
    }

    if (s1[i] == 'A') {
      if (s2[i] == 'G') {
        ++transitions;
      } else {
        ++transversions;
      }
    } else if (s1[i] == 'C') {
      if (s2[i] == 'T') {
        ++transitions;
      } else {
        ++transversions;
      }
    } else if (s1[i] == 'G') {
      if (s2[i] == 'A') {
        ++transitions;
      } else {
        ++transversions;
      }
    } else if (s1[i] == 'T') {
      if (s2[i] == 'C') {
        ++transitions;
      } else {
        ++transversions;
      }
    }
  }

  return static_cast<double>(transitions) / static_cast<double>(transversions);
}

double get_proportion_distance(const string& s1, const string& s2) {
  int length = s1.size();
  int diff =
      count_if(xrange(length), [&s1, &s2](int i) { return s1[i] != s2[i]; });
  return static_cast<double>(diff) / static_cast<double>(length);
}
}
