#include "mmy.h"

#include "network.h"

#include <curl/curl.h>

using namespace std;
using namespace mmy;

namespace {
string contents;
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata) {
  size_t numbytes = size * nmemb;
  contents = {reinterpret_cast<const char*>(ptr), numbytes};
  return numbytes;
}
}

namespace network {
string get_url_contents(const string& url) {
  contents.clear();

  CURL* curl = curl_easy_init();
  if (!curl) {
    return contents;
  }

  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

  curl_easy_perform(curl);
  curl_easy_cleanup(curl);

  return contents;
}
}
