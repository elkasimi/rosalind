#include "mmy.h"

#include "permutation_solver.h"

using namespace std;
using namespace mmy;

permutation_solver::permutation_solver(int n, bool negative)
    : n(n), negative(negative), used(n, false) {}

void permutation_solver::find_permutations_dfs(int r) {
  if (r == 0) {
    permutations.push_back(permutation);
    return;
  }

  for (int i : xrange(n)) {
    if (used[i]) {
      continue;
    }

    for (int a : {-i - 1, i + 1}) {
      if (a < 0 && !negative) {
        continue;
      }

      used[i] = true;
      permutation.push_back(a);
      find_permutations_dfs(r - 1);
      permutation.pop_back();
      used[i] = false;
    }
  }
}

void permutation_solver::find_all_permutations() { find_permutations_dfs(n); }
