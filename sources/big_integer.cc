#include "mmy.h"

#include "big_integer.h"

using namespace std;
using namespace mmy;

namespace {
const int base = 10;

vector<int8_t> create_data(const string& s) {
  vector<int8_t> data(s.size());
  transform(s, data, [](char c) { return static_cast<int8_t>(c - '0'); });

  reverse(data);

  return data;
}
}

big_integer::big_integer(const big_integer& rhs) : data(rhs.data) {}

big_integer::big_integer(big_integer&& rvalue) : data(std::move(rvalue.data)) {}

big_integer& big_integer::operator=(big_integer&& rvalue) {
  data = std::move(rvalue.data);
  return *this;
}

big_integer::big_integer(const string& s) : data(create_data(s)) {}

big_integer& big_integer::operator*=(int n) {
  int r = 0;
  vector<int8_t> tmp_data;
  for (int i : data) {
    int x = n * i + r;
    int8_t value = static_cast<int8_t>(x % base);
    r = x / base;
    tmp_data.push_back(value);
  }

  for (; r > 0; r /= base) {
    tmp_data.push_back(static_cast<int8_t>(r % base));
  }

  data = std::move(tmp_data);
  return *this;
}

big_integer big_integer::operator*(int n) const {
  big_integer result = *this;
  result *= n;

  return result;
}

big_integer& big_integer::operator/=(int n) {
  int64_t value = 0;
  vector<int8_t> tmp_data;
  for (int8_t i : reversed(data)) {
    value = base * value + i;
    int8_t digit = static_cast<int8_t>(value / n);
    if (!tmp_data.empty() || digit > 0) {
      tmp_data.push_back(digit);
    }
    value %= n;
  }

  if (tmp_data.empty()) {
    tmp_data.push_back(0);
  }

  reverse(tmp_data);

  data = std::move(tmp_data);

  return *this;
}

big_integer big_integer::operator/(int n) const {
  big_integer result = *this;
  result /= n;

  return result;
}

big_integer& big_integer::operator+=(const big_integer& rhs) {
  vector<int8_t> tmp_data;
  int i = 0;
  int length = static_cast<int>(data.size());
  int rhs_length = static_cast<int>(rhs.data.size());
  int r = 0;

  for (; i < std::min(length, rhs_length); ++i) {
    int x = r + static_cast<int>(data[i]) + static_cast<int>(rhs.data[i]);
    tmp_data.push_back(static_cast<int8_t>(x % base));
    r = x / base;
  }

  for (; i < length; ++i) {
    int x = r + static_cast<int>(data[i]);
    tmp_data.push_back(static_cast<int8_t>(x % base));
    r = x / base;
  }

  for (; i < rhs_length; ++i) {
    int x = r + static_cast<int>(rhs.data[i]);
    tmp_data.push_back(static_cast<int8_t>(x % base));
    r = x / base;
  }

  if (r > 0) {
    tmp_data.push_back(r);
  }

  data = std::move(tmp_data);
  return *this;
}

big_integer big_integer::operator+(const big_integer& rhs) const {
  big_integer result = *this;
  result += rhs;

  return result;
}

big_integer big_integer::factorial(int n) {
  big_integer factorial_n{"1"};
  for (int i : xrange(2, n + 1)) {
    factorial_n *= i;
  }

  return factorial_n;
}

big_integer big_integer::combination(int n, int k) {
  if (n < k) {
    return big_integer{"0"};
  }

  big_integer result{"1"};
  for (int i : xrange(k)) {
    result *= n - i;
  }

  for (int i : xrange(k)) {
    result /= i + 1;
  }

  return result;
}

istream& operator>>(istream& in, big_integer& bi) {
  string s;
  in >> s;
  bi = big_integer(s);
  return in;
}

ostream& operator<<(ostream& out, const big_integer& bi) {
  for (int8_t i : reversed(bi.data)) {
    out << static_cast<int>(i);
  }

  return out;
}
